import { Application } from "express";
import specialty from "../controllers/speciality.controller";
import validation from "../validations/specialty.validation";

export const specialityRoutes = (app: Application) => {
  app
    .route("/v1/specialty")
    .get(specialty.list)
    .post(validation.create, specialty.create);

  app
    .route("/v1/specialty/:specialty")
    .get(specialty.show)
    .put(validation.update, specialty.update)
    .delete(specialty.remove);

  app.param("specialty", specialty.param);
};

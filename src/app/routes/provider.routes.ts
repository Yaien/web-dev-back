import { Application } from "express";
import provider from "../controllers/provider.controller";
import validation from "../validations/provider.validation";

export const providerRoutes = (app: Application) => {
  app
    .route("/v1/provider")
    .get(provider.list)
    .post(validation.create, provider.create);

  app
    .route("/v1/provider/:provider")
    .get(provider.show)
    .put(validation.update, provider.update)
    .delete(provider.remove);

  app.param("provider", provider.param);
};

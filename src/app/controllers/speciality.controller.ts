import { RequestHandler, RequestParamHandler } from "express";
import { Specialty } from "../models";

const create: RequestHandler = async (req, res, next) => {
  try {
    let speciality = await Specialty.create(req.body);
    res.send(speciality);
  } catch (err) {
    next(err);
  }
};

const list: RequestHandler = async (req, res, next) => {
  try {
    let specialities = await Specialty.find();
    res.send(specialities);
  } catch (err) {
    next(err);
  }
};

const param: RequestParamHandler = async (req, res, next, _id) => {
  try {
    let query = { _id, deletedAt: null };
    let speciality = await Specialty.findOne(query).orFail();
    res.locals.speciality = speciality;
    next();
  } catch {
    res.status(404).send({ code: "NOT_FOUND" });
  }
};

const show: RequestHandler = async (req, res) => {
  res.send(res.locals.speciality);
};

const remove: RequestHandler = async (req, res) => {
  let speciality: Specialty = res.locals.speciality;
  await speciality.remove();
  res.send(speciality);
};

const update: RequestHandler = async (req, res) => {
  let speciality: Specialty = res.locals.speciality;
  speciality.set(req.body);
  await speciality.save();
  res.send(speciality);
};

export default {
  show,
  remove,
  param,
  create,
  list,
  update
};

import { Specialty } from "../../models";
import specialty from "../speciality.controller";

describe("SpecialtyController", () => {
  let res: any = {
    locals: {
      speciality: new Specialty()
    },
    send: jest.fn(),
    status: jest.fn(function() {
      return this;
    })
  };

  let next = jest.fn();

  it("Should remove", async () => {
    spyOn(res.locals.speciality, "remove").and.returnValue(Promise.resolve());
    await specialty.remove(null, res, next);
    expect(res.send).toHaveBeenCalledWith(res.locals.speciality);
  });

  it("Param Middleware Should bind current provider", async () => {
    let current = new Specialty();
    let id = "xxx";
    let find = spyOn(Specialty, "findOne").and.returnValue({
      orFail: () => Promise.resolve(current)
    });

    await specialty.param(null, res, next, id, null);

    expect(find).toHaveBeenCalled();
    expect(res.locals).toHaveProperty("speciality", current);
    expect(next).toHaveBeenCalled();
  });

  it("Param Middleware Should response 404 on missing", async () => {
    let find = spyOn(Specialty, "findOne").and.throwError(null);
    let id = "xxx";
    await specialty.param(null, res, next, id, null);

    expect(find).toHaveBeenCalled();
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.send).toHaveBeenCalled();
  });

  it("Should update provider", async () => {
    let save = spyOn(res.locals.speciality, "save").and.returnValue(
      Promise.resolve()
    );

    let name = "Fixing Things";

    let req: any = {
      body: {
        name
      }
    };

    await specialty.update(req, res, next);

    expect(res.locals.speciality).toHaveProperty("name", name);
    expect(save).toHaveBeenCalled();
  });

  it("Should create", async () => {
    let name = "Fix";

    let req: any = {
      body: {
        name
      }
    };

    let speciality = new Specialty({ name: "name" });

    let create = spyOn(Specialty, "create").and.returnValue(
      Promise.resolve(speciality)
    );

    await specialty.create(req, res, next);

    expect(res.send).toHaveBeenCalledWith(speciality);
  });
});

import provider from "../provider.controller";
import { Provider, Specialty } from "../../models";

describe("ProviderController", () => {
  let res: any = {
    locals: {
      provider: new Provider()
    },
    send: jest.fn(),
    status: jest.fn(function() {
      return this;
    })
  };

  let next = jest.fn();

  it("Should remove provider", async () => {
    spyOn(res.locals.provider, "remove").and.returnValue(Promise.resolve());
    await provider.remove(null, res, next);
    expect(res.send).toHaveBeenCalledWith(res.locals.provider);
  });

  it("Param Middleware Should bind current provider", async () => {
    let currentProvider = new Provider({ firstName: "Daniel" });
    let id = "xxx";
    let find = spyOn(Provider, "findById").and.returnValue({
      orFail: () => Promise.resolve(currentProvider)
    });

    await provider.param(null, res, next, id, null);

    expect(find).toHaveBeenCalledWith(id);
    expect(res.locals).toHaveProperty("provider", currentProvider);
    expect(next).toHaveBeenCalled();
  });

  it("Param Middleware Should response 404 on missing provider", async () => {
    let find = spyOn(Provider, "findById").and.throwError(null);
    let id = "xxx";
    await provider.param(null, res, next, id, null);

    expect(find).toHaveBeenCalledWith(id);
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.send).toHaveBeenCalled();
  });

  it("Should update provider", async () => {
    let save = spyOn(res.locals.provider, "save").and.returnValue(
      Promise.resolve()
    );

    let firstName = "firstName";

    let req: any = {
      body: {
        firstName
      }
    };

    await provider.update(req, res, next);

    expect(res.locals.provider).toHaveProperty("firstName", firstName);
    expect(save).toHaveBeenCalled();
  });

  it("Should update provider specialty", async () => {
    let specialty = new Specialty({ name: "specialty" });

    let findById = spyOn(Specialty, "findById").and.returnValue(
      Promise.resolve(specialty)
    );

    let save = spyOn(res.locals.provider, "save").and.returnValue(
      Promise.resolve()
    );

    let req: any = { body: { specialty: "xxx" } };

    await provider.update(req, res, next);

    expect(findById).toHaveBeenCalledWith(req.body.specialty);
    expect(res.locals.provider.specialty.name).toBe(specialty.name);
    expect(save).toHaveBeenCalled();
  });

  it("Should send update failed response on missing specialty", async () => {
    let findById = spyOn(Specialty, "findById").and.returnValue(
      Promise.resolve(null)
    );

    let req: any = { body: { specialty: "xxx" } };

    await provider.update(req, res, next);

    expect(findById).toHaveBeenCalledWith(req.body.specialty);
    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.send).toHaveBeenCalled();
  });

  it("Should create provider", async () => {
    let firstName = "firstName";

    let req: any = {
      body: {
        firstName,
        specialty: "xxx"
      }
    };

    let newProvider = new Provider(req.body);
    let specialty = new Specialty({ name: "name" });

    spyOn(Provider, "create").and.returnValue(Promise.resolve(newProvider));
    spyOn(Specialty, "findById").and.returnValue(Promise.resolve(specialty));

    await provider.create(req, res, next);

    expect(res.send).toHaveBeenCalledWith(newProvider);
  });
});

import { RequestHandler, RequestParamHandler } from "express";
import { Provider, Specialty } from "../models";

const create: RequestHandler = async (req, res, next) => {
  try {
    let specialty = await Specialty.findById(req.body.specialty);
    if (!specialty) {
      return res.status(400).send({ specialty: ["Speciality Not Found"] });
    }
    let provider = await Provider.create({ ...req.body, specialty });
    res.send(provider);
  } catch (err) {
    next(err);
  }
};

const list: RequestHandler = async (req, res, next) => {
  try {
    let providers = await Provider.find();
    res.send(providers);
  } catch (err) {
    next(err);
  }
};

const param: RequestParamHandler = async (req, res, next, _id) => {
  try {
    let provider = await Provider.findById(_id).orFail();
    res.locals.provider = provider;
    next();
  } catch {
    res.status(404).send({ code: "NOT_FOUND_PROVIDER" });
  }
};

const show: RequestHandler = async (req, res) => {
  res.send(res.locals.provider);
};

const remove: RequestHandler = async (req, res, next) => {
  try {
    let provider: Provider = res.locals.provider;
    await provider.remove();
    res.send(provider);
  } catch (err) {
    next(err);
  }
};

const update: RequestHandler = async (req, res, next) => {
  try {
    let { specialty, ...attrs } = req.body;
    let provider: Provider = res.locals.provider;

    if (specialty) {
      let specialtyDocument = await Specialty.findById(specialty);
      if (!specialtyDocument) {
        return res.status(400).send({ specialty: ["Speciality Not Found"] });
      }

      provider.specialty = specialtyDocument;
    }
    provider.set(attrs);
    await provider.save();
    res.send(provider);
  } catch (err) {
    next(err);
  }
};

export default {
  show,
  remove,
  param,
  create,
  list,
  update
};

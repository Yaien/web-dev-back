import { validation } from "../validation.middleware";

describe("Validation Middleware", () => {
  let response = (): any => ({
    send: jest.fn(),
    status: jest.fn(function() {
      return this;
    })
  });

  let next = jest.fn();

  let validate = validation({
    name: {
      presence: true
    },
    age: {
      numericality: true,
      type: Number
    }
  });

  it("Should validate and cast request input values", () => {
    let req: any = { body: { name: "x", age: "20" } };
    let res = response();
    validate(req, res, next);
    expect(next).toHaveBeenCalled();
    expect(req.body).toEqual({ name: "x", age: 20 });
  });

  it("Should response 400 on invalid input", () => {
    let req: any = { body: { age: "xxxsds" } };
    let res = response();
    validate(req, res, next);
    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.send).toHaveBeenCalled();
    expect(next).not.toHaveBeenCalled();
  });

  afterEach(() => jest.resetAllMocks());
});

import { validate, extend, validators } from "validate.js";
import { RequestHandler } from "express";

/**
 * Cast al attribututes to his current type
 * @example { number: "0" } becomes { number: 0 }
 * @param attrs
 * @param constraints
 */
function cast(attrs: object, constraints: any) {
  let result = {};
  for (let key of Object.keys(constraints)) {
    let value = attrs[key];
    if (value != undefined) {
      let type = constraints[key].type;
      if (type) {
        value = type(value);
      }
      result[key] = value;
    }
    delete constraints[key].type;
  }
  return result;
}

extend(validators.datetime, {
  // The value is guaranteed not to be null or undefined but otherwise it
  // could be anything.
  parse: function(value: string, options: any) {
    return new Date(value);
  },
  // Input is a ISO-8601 timestamp
  format: function(value: string, options: any) {
    return new Date(value);
  }
});

export function validation(constraints: any): RequestHandler {
  return (req, res, next) => {
    let attrs = cast(req.body, constraints);

    let errors = validate(attrs, constraints, {
      fullMessages: false
    });

    if (errors) {
      return res.status(400).send(errors);
    }

    req.body = attrs;
    next();
  };
}

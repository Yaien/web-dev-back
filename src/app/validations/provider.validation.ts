import { validation } from "../middlewares/validation.middleware";

const create = validation({
  firstName: {
    presence: { allowEmpty: false }
  },
  lastName: {
    presence: { allowEmpty: false }
  },
  middleName: {},
  email: {
    presence: true,
    email: true
  },
  specialty: {
    presence: true,
    format: /^[0-9a-fA-F]{24}$/
  },
  projectedStartDate: {
    presence: true,
    datetime: true,
    type: Date
  },
  employerId: {
    presence: true,
    type: Number,
    numericality: true
  },
  providerType: {
    inclusion: ["MD", "DO", "PA", "DPM", "ARNP", "NP", "TEST PUT", "TEST"],
    presence: true
  },
  staffStatus: {
    presence: true,
    inclusion: [
      "ASSOCIATE",
      "ACTIVE",
      "COURTESY",
      "COMMUNITY",
      "AFFILIATE",
      "TEACHING",
      "TEST PUT",
      "TEST",
      "CONSULTING",
      "HONORARY",
      "PROVISIONAL"
    ]
  },
  createdBy: {
    presence: true,
    type: Number,
    numericality: true
  },
  assignedTo: {
    presence: true,
    type: Number,
    numericality: true
  }
});

const update = validation({
  firstName: {},
  lastName: {},
  middleName: {},
  email: {
    email: true
  },
  specialty: {
    format: /^[0-9a-fA-F]{24}$/
  },
  projectedStartDate: {
    datetime: true,
    type: Date
  },
  employerId: {
    type: Number
  },
  providerType: {
    inclusion: ["MD", "DO", "PA", "DPM", "ARNP", "NP", "TEST PUT", "TEST"]
  },
  staffStatus: {
    inclusion: [
      "ASSOCIATE",
      "ACTIVE",
      "COURTESY",
      "COMMUNITY",
      "AFFILIATE",
      "TEACHING",
      "TEST PUT",
      "TEST",
      "CONSULTING",
      "HONORARY",
      "PROVISIONAL"
    ]
  },
  updatedBy: {
    type: Number,
    numericality: true
  },
  assignedTo: {
    type: Number
  }
});

export default { create, update };

import { validation } from "../middlewares/validation.middleware";

const create = validation({
  name: {
    presence: { allowEmpty: false }
  },
  createdBy: {
    presence: true,
    type: Number,
    numericality: true
  }
});

const update = validation({
  name: {},
  updatedBy: {
    presence: true,
    type: Number,
    numericality: true
  }
});

export default { create, update };

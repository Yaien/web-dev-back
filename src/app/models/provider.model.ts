import { Schema, model, Document, Model, Query } from "mongoose";
import { Specialty, specialitySchema } from "./specialty.model";

export interface Provider extends Document {
  firstName: string;
  lastName: string;
  middleName: string;
  email: string;
  specialty: Specialty;
  projectedStartDate: Date;
  employerId: number;
  providerType: string;
  staffStatus: string;
  createdBy: number;
  createdAt: Date;
  updatedBy: number;
  assignedTo: number;
  updatedAt: Date;
}

interface ProviderModel extends Model<Provider> {
  active(): Query<Provider>;
}

export const providerSchema = new Schema({
  firstName: String,
  lastName: String,
  middleName: String,
  email: String,
  specialty: specialitySchema,
  projectedStartDate: Date,
  employerId: Number,
  providerType: String,
  staffStatus: String,
  createdAt: { type: Date, default: Date.now },
  createdBy: Number,
  updatedAt: Date,
  updatedBy: Number,
  assignedTo: Number
});

providerSchema.pre("save", function(this: Provider) {
  if (!this.isNew) {
    this.updatedAt = new Date();
  }
});

export const Provider = model<Provider, ProviderModel>(
  "Provider",
  providerSchema,
  "providers"
);

import { Schema, Document, model, Model, Query } from "mongoose";

export interface Specialty extends Document {
  name: string;
  createdAt: Date;
  createdBy: Date;
  updatedAt: Date;
  updatedBy: Date;
}

interface SpecialityModel extends Model<Specialty> {
  active(): Query<Specialty>;
}

export const specialitySchema = new Schema({
  name: { type: String, required: true },
  createdAt: { type: Date, default: Date.now },
  createdBy: Number,
  updatedAt: Date,
  updatedBy: Number
});

specialitySchema.pre("save", function(this: Specialty) {
  if (!this.isNew) {
    this.updatedAt = new Date();
  }
});

export const Specialty = model<Specialty, SpecialityModel>(
  "Specialty",
  specialitySchema,
  "specialties"
);

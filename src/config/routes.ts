import { Application } from "express";
import { specialityRoutes } from "../app/routes/speciality.routes";
import { providerRoutes } from "../app/routes/provider.routes";

export default (app: Application) => {
  specialityRoutes(app);
  providerRoutes(app);
};

import dotenv from "dotenv";
dotenv.config();

export interface Env {
  production?: boolean;
  host: string;
  port: number | string;
  mongo: string;
}

export const dev: Env = {
  host: process.env.HOST,
  port: process.env.PORT || 3000,
  mongo: process.env.MONGODB_URI
};

export const prod: Env = {
  production: true,
  host: process.env.HOST,
  port: process.env.PORT || 8080,
  mongo: process.env.MONGODB_URI
};

export function load() {
  return process.env.NODE_ENV == "production" ? prod : dev;
}

export default load();

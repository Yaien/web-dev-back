import { load, dev, prod } from "../env";

describe("Environment Config", () => {
  it("Should be dev on default environment", () => {
    process.env.NODE_ENV = null;
    expect(load()).toBe(dev);
  });

  it("Should be prod on production environment", () => {
    process.env.NODE_ENV = "production";
    expect(load()).toBe(prod);
  });

  afterEach(() => delete process.env.NODE_ENV);
});

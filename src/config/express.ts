import express from "express";
import bodyParser from "body-parser";
import morgan from "morgan";
import compression from "compression";
import methodOverride from "method-override";
import helmet from "helmet";
import env from "./env";
import routes from "./routes";

export default () => {
  let app = express();
  app.use(env.production ? compression() : morgan("dev"));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(methodOverride());
  app.use(helmet());
  app.use(express.static("public"));
  app.set("view engine", "pug");
  app.set("views", "templates");
  routes(app);
  return app;
};

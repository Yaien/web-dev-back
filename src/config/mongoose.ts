import mongoose from "mongoose";
import env from "./env";

export default () => {
  console.log("Connecting to " + env.mongo);
  return mongoose.connect(env.mongo, {
    useNewUrlParser: true,
    useCreateIndex: true
  });
};

export function close() {
  return mongoose.disconnect();
}

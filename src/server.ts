import express from "./config/express";
import env from "./config/env";
import mongoose from "./config/mongoose";

const app = express();

async function init() {
  await mongoose();
  app.listen(env.port, () => {
    console.log("Server listening on port " + env.port);
  });
}

init();
